CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting

INTRODUCTION
------------

The Domain Cloudfront Edge Caching allows clear cache pages in Amazon services
through the Drupal Interface.
This module extends domain module with cloudfront.

REQUIREMENTS
------------

This module requires the following SDK:

 * AWS SDK PHP (https://github.com/aws/aws-sdk-php)
 * Domain access module


INSTALLATION
------------
 * Remember install aws-sdk-php before enable the module.
 * If you are installing through composer then it will take care of all the
 depedencies.
 * Please make sure all the dependecies are met before enabling the module.

CONFIGURATION
--------------
 * Make sure domain records are added
 * Add Aws credentials for application to connect to aws:
 /admin/config/services/domain_cec
 * Add distribution ID for domains:
 /admin/config/services/domain_cec/distribution_ids
 * You can clear cache by going to :
/admin/conent/domain_cloud_front_cache_clear


Troubleshooting
-----------------
 * Check if aws credentials are correct.
 * Check Distribution id entered is correct
 * Clear cache after module install
