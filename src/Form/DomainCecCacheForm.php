<?php

namespace Drupal\domain_cf_edge_caching\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;

/**
 * @file
 * Contains \Drupal\domain_cf_edge_caching\Form\CecAdminSettingsForm.
 */

/**
 * Reference update for domains content.
 */
class DomainCecCacheForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_cloud_front_cache_clear_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $credintials_config = \Drupal::config('domain_cec.settings');
    if (empty($credintials_config->get('dcec_key'))) {
      $link = Link::fromTextAndUrl($this->t('Domain Cloudfront Edge Caching'), Url::fromRoute('dcec.admin'))->toString();
      $form['error']['#markup'] = $this->t('@link configuration values are required for the cloudfront service.', ['@link' => $link]);
    }
    else {
      $domains_opt = [];
      $domains_opt = \Drupal::entityTypeManager()->getStorage('domain')->loadOptionsList();
      $form['cache_domain_id'] = [
        '#type' => 'select',
        '#title' => $this->t('Select Domain for Clear Cache on Cloud front.'),
        '#required' => TRUE,
        '#options' => $domains_opt,
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $domain_id = $form_state->getValue('cache_domain_id');
    $config = \Drupal::config('domain_cec_distribution_id.settings');
    $distribution_id = $config->get($domain_id . '_distribution_id');
    if (empty($distribution_id)) {
      $form_state->setErrorByName('cache_domain_id', $this->t('Please add the Distribution Id for domain @domain_id.', ['@domain_id' => $domain_id]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $domain_id = $form_state->getValue('cache_domain_id');
    $config = \Drupal::config('domain_cec_distribution_id.settings');
    $distribution_id = $config->get($domain_id . '_distribution_id');
    if (empty($distribution_id)) {
      \Drupal::messenger()->addMessage($this->t('Please add the Distribution Id for domain @domain_id.', ['@domain_id' => $domain_id]), 'error');
    }
    else {
      // Call cache invalidate function.
      $path = ['/*'];
      domain_cf_edge_caching_invalidate_url($path, $distribution_id, $domain_id);
      $form_state->setRedirect('dcec.cloudcache_clear');
    }
  }

}
