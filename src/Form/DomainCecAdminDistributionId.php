<?php

namespace Drupal\domain_cf_edge_caching\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Configure settings for Cloudfront credentials.
 */
class DomainCecAdminDistributionId extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_cec_distribution_ids';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'domain_cec_distribution_id.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('domain_cec_distribution_id.settings');

    // Distribution ID.
    $domains_opt = \Drupal::entityTypeManager()->getStorage('domain')->loadOptionsList();
    if (!empty($domains_opt)) {
      $form['settings'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Distribution Ids Settings'),
      ];
      foreach ($domains_opt as $key => $value) {
        $form['settings'][$key . '_distribution_id'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Distribution ID For @value', ['@value' => $value]),
          '#default_value' => $config->get($key . '_distribution_id'),
          '#size' => 20,
          '#maxlength' => 128,
          '#description' => $this->t('Ej: E206SWIPUZ2Z48'),
        ];
      }
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#button_type' => 'primary',
      ];

      return parent::buildForm($form, $form_state);
    }
    else {
      $link = Link::fromTextAndUrl($this->t('Add Domain'), Url::fromUri('internal:/admin/config/domain'))->toString();
      $form['error']['#markup'] = $this->t('Please @link for adding distribution ids.', ['@link' => $link]);
      return $form;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $domains_opt = \Drupal::entityTypeManager()->getStorage('domain')->loadOptionsList();

    // Retrieve the configuration.
    foreach ($domains_opt as $key => $value) {
      $this->config('domain_cec_distribution_id.settings')
        // Set the submitted configuration setting.
        ->set($key . '_distribution_id', $form_state->getValue($key . '_distribution_id'))
        ->save();
    }

    parent::submitForm($form, $form_state);
  }

}
