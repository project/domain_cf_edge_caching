<?php

namespace Drupal\domain_cf_edge_caching\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for Cloudfront credentials.
 */
class DomainCecAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_cec_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'domain_cec.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('domain_cec.settings');

    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings'),
    ];

    // Region.
    $form['settings']['dcec_region'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Region'),
      '#default_value' => $config->get('dcec_region'),
      '#size' => 10,
      '#maxlength' => 128,
      '#description' => $this->t('Example: us-east-1'),
      '#required' => TRUE,
    ];

    // Key.
    $form['settings']['dcec_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Key Id'),
      '#default_value' => $config->get('dcec_key'),
      '#size' => 20,
      '#maxlength' => 128,
      '#description' => $this->t('Example: EOjWGh6Keft9czeNkmHsa1aMcrhYukxdlIXRayDt'),
      '#required' => TRUE,
    ];

    // Secret.
    $form['settings']['dcec_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Access Key'),
      '#default_value' => $config->get('dcec_secret'),
      '#size' => 50,
      '#maxlength' => 128,
      '#description' => $this->t('Example: AHIAJF6JNSRJRVNSDOKA'),
      '#required' => TRUE,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Get data.
    $data = $form_state->getValues();

    // Test connection.
    $test_connection_cec = domain_cf_edge_caching_test_connection($data['dcec_region'], $data['dcec_key'], $data['dcec_secret']);

    if ($test_connection_cec[0] == FALSE) {
      switch ($test_connection_cec[1]) {
        case '403':
          $form_state->setErrorByName('dcec_key', $this->t('The credentials are incorrect.'));
          $form_state->setErrorByName('dcec_secret', $this->t('The credentials are incorrect.'));
          break;

        default:
          $form_state->setErrorByName('', $this->t('@field_value', ['@field_value' => $test_connection_cec[2]]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config('domain_cec.settings')
      // Set the submitted configuration setting.
      ->set('dcec_region', $form_state->getValue('dcec_region'))
      ->set('dcec_key', $form_state->getValue('dcec_key'))
      ->set('dcec_secret', $form_state->getValue('dcec_secret'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
